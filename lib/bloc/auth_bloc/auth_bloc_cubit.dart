import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:majootestcase/common/widget/alert_dialog.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/hive.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    // Box<bool> _box = Hive.box("local-setting");

    emit(AuthBlocInitialState());
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = await LocalHive.i.getStatusUser();

    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  ///Ganti state auth ke register
  ///@author nur syahfei
  void goToRegister() {
    emit(AuthBlocRegisterState());
  }

  ///ganti state auth ke login
  ///@author nur syahfei
  void goToLogin() {
    emit(AuthBlocLoginState());
  }

  ///Digunakan untuk melakukan register akun baru
  ///@author nur syahfei
  ///@param Srting _email:  email user
  ///@param Srting  _password:  password user
  ///@param Srting  _username:  username  user
  void register(String? _email, String? _password, String _username, {bool? valid}) async {
    if (_email == null || _password == null) {
      await _showAlert(
        title: "Login Gagal",
        content: "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
        isError: true,
      );
      return;
    }

    if (!(valid ?? false)) {
      await _showAlert(title: "Login Gagal", content: "Masukkan e-mail yang valid", isError: true);
      return;
    }

    final user = await LocalHive.i.insertUser(User(email: _email, password: _password, userName: _username, id: null));
    if (user != null) {
      // String data = jsonEncode(user.toJson());

      await LocalHive.i.insertUserLogin(user);

      await _showAlert(title: "Register Berhasil", content: "", isError: false);

      // sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState());
    } else {
      await _showAlert(title: "Register Gagal", content: "", isError: true);
      emit(AuthBlocLoginState());
    }
  }

  ///Digunakan login ke aplikasi
  ///@author nur syahfei
  ///@param Srting _email:  email user
  ///@param Srting  _password:  password user
  ///@param Srting  _username:  username  user
  void loginUser(String? _email, String? _password) async {
    if (_email == null || _password == null) {
      await _showAlert(
        title: "Login Gagal",
        content: "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
        isError: true,
      );
      return;
    }

    final user = await LocalHive.i.login(email: _email, password: _password);
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (user != null) {
      // await sharedPreferences.setBool("is_logged_in", true);
      // String data = jsonEncode(user.toJson());
      await LocalHive.i.insertUserLogin(user);

      await _showAlert(title: "Login Berhasil", content: "", isError: false);

      emit(AuthBlocLoggedInState());
    } else {
      await _showAlert(title: "Login Gagal", content: "Periksa kembali inputan anda", isError: true);
      emit(AuthBlocLoginState());
    }
  }

  ///Digunakan logout ke aplikasi, dan menghapus setting di sharedPreferences
  ///@author nur syahfei
  void logout() async {
    emit(AuthBlocLoadingState());
    await LocalHive.i.logOutUser();
    emit(AuthBlocLoginState());
  }

  ///Digunakan menampilkan dialog alaert gagal atau berhasil
  ///@author nur syahfei
  ///@param String title: judul alert
  ///@param String content: isi alart
  ///@param bool isError: menemtukan apakah ini dialog error atau berhasil
  Future<void> _showAlert({String? title, String? content, bool? isError}) async {
    if (navigatorKey.currentContext != null) {
      await showDialog(
        context: navigatorKey.currentContext!,
        builder: (_) => CostumeAlertDialog(content: content, title: title, isError: isError),
      );
    }
  }
}
