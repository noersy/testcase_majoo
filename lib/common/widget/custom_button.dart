import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

abstract class _Button extends StatelessWidget {
  const _Button({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class CostumButton extends StatelessWidget {
  final String? text;
  final double height;
  final VoidCallback? onPressed;
  final Widget? leadingIcon;
  final OutlinedBorder? shapeBorder;
  final bool isSecondary;
  final bool isOutline;

  const CostumButton({
    Key? key,
    @required this.text,
    this.height = 40,
    this.onPressed,
    this.leadingIcon,
    this.shapeBorder,
    this.isSecondary = false,
  })  : isOutline = false,
        super(key: key);

  const CostumButton.outline({
    Key? key,
    this.text,
    this.height = 40,
    this.onPressed,
    this.leadingIcon,
    this.shapeBorder,
    this.isSecondary = false,
  })  : isOutline = true,
        super(key: key);

  OutlinedBorder _shapeBorder(BuildContext context) {
    return shapeBorder ??
        RoundedRectangleBorder(
          side: isSecondary
              ? BorderSide(
                  color: Theme.of(context).unselectedWidgetColor,
                  width: 1,
                  style: BorderStyle.solid,
                )
              : BorderSide.none,
          borderRadius: BorderRadius.circular(8),
        );
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final color = isSecondary ? Theme.of(context).unselectedWidgetColor : primaryColor;

    if (isOutline) {
      return ButtonTheme(
          height: height,
          buttonColor: color,
          minWidth: MediaQuery.of(context).size.width,
          child: OutlinedButton.icon(
            style: OutlinedButton.styleFrom(
                backgroundColor: color,
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: EdgeInsets.symmetric(vertical: SpDims.sp12),
                shape: _shapeBorder(context),
                side: BorderSide(color: Theme.of(context).colorScheme.onBackground, width: 2)),
            icon: leadingIcon ?? const SizedBox.shrink(),
            label: SizedBox(
              width: double.infinity,
              child: Text(
                text ?? '',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
              ),
            ),
            onPressed: onPressed,
          ));
    } else {
      return ButtonTheme(
        height: height,
        buttonColor: color,
        minWidth: MediaQuery.of(context).size.width,
        child: TextButton.icon(
          style: TextButton.styleFrom(
            backgroundColor: color,
            padding: EdgeInsets.symmetric(vertical: SpDims.sp12),
            shape: _shapeBorder(context),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onSurface: Colors.grey,
          ),
          icon: leadingIcon ?? const SizedBox.shrink(),
          label: SizedBox(
            width: double.infinity,
            child: Text(
              text ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(color: Theme.of(context).colorScheme.onBackground),
            ),
          ),
          onPressed: onPressed,
        ),
      );
    }
  }
}
