import 'package:hive/hive.dart';
part 'user.g.dart';

@HiveType(typeId: 0)
class User extends HiveObject {
  @HiveField(0)
  String? email;
  @HiveField(1)
  String? userName;
  @HiveField(2)
  String? password;
  @HiveField(3)
  int? id;

  User({
    required this.id,
    required this.userName,
    required this.email,
    required this.password,
  });

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        id = json['id'],
        userName = json['username'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': email,
        'password': password,
        'username': userName,
      };

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{columnId: id, columnEmail: email, columnPassword: password, columnUsername: userName};
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  User.fromMap(Map<String, dynamic>? map) {
    id = map?[columnId];
    email = map?[columnEmail];
    userName = map?[columnUsername];
    password = map?[columnPassword];
  }

  static const String tableUser = 'user';
  static const String columnId = 'id';
  static const String columnEmail = 'email';
  static const String columnUsername = 'username';
  static const String columnPassword = 'password';
}
