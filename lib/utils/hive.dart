import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:majootestcase/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class LocalHive {
  static final LocalHive _singleton = LocalHive._internal();

  LocalHive._internal();

  static LocalHive get i => _singleton;

  Box? _boxSetting;
  Box<User>? _boxUsers;

  Future<void> initialize() async {
    if (await Permission.storage.request().isGranted) {
      Directory root = await getApplicationDocumentsDirectory();
      Hive.init(root.path + '/hive');
      Hive.registerAdapter(UserAdapter());
    }

    _boxSetting = await Hive.openBox('local-setting');
    _boxUsers = await Hive.openBox('local-users');

    // if (!(_box?.containsKey("majoo@gmail.com") ?? true)) {
    _boxUsers?.put(
        "majoo@gmail.com",
        User(
          id: 0,
          userName: 'majoo',
          email: 'majoo@gmail.com',
          password: '123456',
        ));
    // }

    // _db = await openDatabase(SqlLite.path, version: 2, onCreate: (Database db, int version) async {
    //   await db.execute('''
    //   create table ${User.tableUser} (
    //   ${User.columnId} integer primary key autoincrement,
    //   ${User.columnEmail} text not null,
    //   ${User.columnUsername} text,
    //   ${User.columnPassword} text not null)
    //   ''');
    // });

    // if (_db != null) {
    //   await _db?.insert(User.tableUser, {
    //     '${User.columnUsername}': "Majoo",
    //     '${User.columnEmail}': "majoo@gmail.com",
    //     '${User.columnPassword}': "123456",
    //   });
    // }
  }

  Future<User?> insertUser(User user) async {
    try {
      _boxUsers?.put(user.email, user);

      // user.id = await _db?.insert(User.tableUser, user.toMap());
      return user;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  Future<User?> insertUserLogin(User user) async {
    try {
      await _boxSetting?.put('user-value', user.email);
      await _boxSetting?.put("is_logged_in", true);

      // user.id = await _db?.insert(User.tableUser, user.toMap());
      return user;
    } on Exception catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  Future<User?> login({required String email, required String password}) async {
    try {
      print(email);
      print(password);

      final user = await _boxUsers?.get(email);

      return user;
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }

  Future<bool?> getStatusUser() async {
    try {
      return await _boxSetting?.get('is_logged_in');
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }

  Future<User?> getUser() async {
    try {
      String value = await _boxSetting?.get("user-value");
      return await _boxUsers?.get(value);
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }

  Future<bool?> logOutUser() async {
    try {
      await _boxSetting?.put("is_logged_in", false);
      await _boxSetting?.put("user-value", null);
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }
}
